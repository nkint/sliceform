import { PI, fit } from '@thi.ng/math'
import { Vec } from '@thi.ng/vectors'

export const paramsFn2 = [{ type: 'range' as const, min: 1, max: 5, value: 1, name: 'α' }]

export const nameFn2 = 'sin( α x y )'

export const getFn2 = (param1: number) => ([x01, y01]: [number, number]) => {
  const x0PI = x01 * PI * param1
  const y0PI = y01 * PI * 1
  const x11 = fit(x01, 0, 1, -1, 1)
  const y11 = fit(y01, 0, 1, -1, 1)
  const z = Math.sin(x0PI * y0PI) * 0.1
  return [x11, z, y11] as Vec
}
