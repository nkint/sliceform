import { Vec, mulN2 } from '@thi.ng/vectors'
import { fit } from '@thi.ng/math'
import { GlobalState, Context } from './global-state'
import { DataMatrix } from './utils/data-matrix'

export type OffsetLine = { line: Vec[]; offset: number }
export type Ticks = {
  p0: number[]
  p1: number[]
}[]

const reduceToPolyline = (direction: 'row' | 'column', svgScale: number) => (
  acc: { array: OffsetLine[]; y: number },
  x: Vec[],
) => {
  const line = x.map(([x, y, z]) => {
    const point = direction === 'row' ? [x, y] : [z, y]
    return mulN2(point, point, svgScale)
  })
  acc.array.push({ line, offset: acc.y })
  acc.y += svgScale + 30 // TODO: add height from convex hull and / or bounding box
  return acc
}

function computeLine2d(line: Vec[][], direction: 'row' | 'column', svgScale: number) {
  return line.reduce(reduceToPolyline(direction, svgScale), {
    array: [] as OffsetLine[],
    y: 0,
  }).array
}

function computeColumnsTicks(rows: Vec[][], columns: Vec[][], svgScale: number) {
  return rows.map((r, i) =>
    columns.map(line => {
      const point = line[i]
      const tickX = point[0] * svgScale
      return {
        p0: [tickX, -svgScale],
        p1: [tickX, -svgScale / 2],
      }
    }),
  )
}

function computeRowsTicks(columns: Vec[][], rows: Vec[][], svgScale: number, data: DataMatrix) {
  return columns.map((c, columnIndex) =>
    rows.map((line, rowIndex) => {
      const point = line[0]
      const tickX = point[2] * svgScale
      const cI = Math.round(fit(columnIndex, 0, columns.length - 1, 0, data.colNum))
      const rI = Math.round(fit(rowIndex, 0, rows.length - 1, 0, data.rowNum))
      const tickY = data.at(cI, rI)[1] * svgScale
      return {
        p0: [tickX, -svgScale / 2],
        p1: [tickX, tickY],
      }
    }),
  )
}

export function allocateMemory(state: GlobalState) {
  const data = {
    rows2d: [] as OffsetLine[],
    columns2d: [] as OffsetLine[],
    rowsTicks: [] as Ticks[],
    columnsTicks: [] as Ticks[],
  }

  return data
}

export function computeNewData(context: Context) {
  const { svgScale } = context.state.deref()
  const { rows, columns, matrix } = context.data3d

  context.data2d.rows2d = computeLine2d(rows, 'row', svgScale)
  context.data2d.columns2d = computeLine2d(columns, 'column', svgScale)

  context.data2d.rowsTicks = computeRowsTicks(columns, rows, svgScale, matrix)
  context.data2d.columnsTicks = computeColumnsTicks(rows, columns, svgScale)
}
