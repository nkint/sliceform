import { ModelSpec, compileModel, GLMat4, Shader } from '@thi.ng/webgl'

export const modelLineBuilder = (
  gl: WebGLRenderingContext,
  positionBuffer: Float32Array,
  indices: Uint16Array,
  uniforms: { proj: GLMat4; view: GLMat4; color: number[] },
  shader: Shader,
) => {
  const modelSpec: ModelSpec = {
    shader,
    uniforms,
    attribs: {
      position: { data: positionBuffer, size: 3 },
    },
    indices: {
      data: indices,
    },
    num: indices.length,
    mode: 1, // gl.LINES
  }
  return compileModel(gl, modelSpec)
}
