import { ShaderSpec } from '@thi.ng/webgl'

export const simpleShader: ShaderSpec = {
  vs: `
    void main() {
      gl_Position = ((proj * (view * model)) * vec4(position, 1.0));
    }`,
  fs: `
    void main() {
      fragColor = color;
    }`,
  attribs: {
    position: 'vec3',
  },
  uniforms: {
    proj: 'mat4',
    view: 'mat4',
    model: 'mat4',
    color: 'vec4',
  },
}
