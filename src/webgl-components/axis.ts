import { mulN3 } from '@thi.ng/vectors'
import { ShaderSpec, compileModel, ModelSpec, shader, GLMat4 } from '@thi.ng/webgl'

const axisGeometry = (gl: WebGLRenderingContext, size: number = 2): Partial<ModelSpec> => {
  const origin = [0, 0, 0]
  const x = [1, 0, 0]
  const y = [0, 1, 0]
  const z = [0, 0, 1]

  return {
    attribs: {
      position: {
        data: new Float32Array([
          ...origin,
          ...mulN3([], x, size),
          ...origin,
          ...mulN3([], y, size),
          ...origin,
          ...mulN3([], z, size),
        ]),
        size: 3,
      },
      color: { data: new Float32Array([...x, ...x, ...y, ...y, ...z, ...z]), size: 3 },
    },
    num: 6,
    mode: gl.LINES,
  }
}

const simpleShaderColor: ShaderSpec = {
  vs: `
    void main() {
      gl_Position = ((proj * (view * model)) * vec4(position, 1.0));
      vColor = color;
    }`,
  fs: `
    void main() {
      fragColor = vec4(vColor, 1.0);
    }`,
  attribs: {
    position: 'vec3',
    color: 'vec3',
  },
  uniforms: {
    proj: 'mat4',
    view: 'mat4',
    model: 'mat4',
  },
  varying: {
    vColor: 'vec3',
  },
}

export const getModelAxis = (gl: WebGLRenderingContext, proj: GLMat4, view: GLMat4) =>
  compileModel(gl, <ModelSpec>{
    shader: shader(gl, simpleShaderColor),
    uniforms: {
      proj,
      view,
    },
    ...axisGeometry(gl),
  })
