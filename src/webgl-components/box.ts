import { ModelSpec, compileModel, shader, GLMat4, Shader } from '@thi.ng/webgl'
import { permutations, flatten } from '@thi.ng/transducers'

const boxGeometry = (gl: WebGLRenderingContext): Partial<ModelSpec> => {
  const [a, b, c, d, e, f, g, h] = [...permutations([-1, 1], [-1, 1], [-1, 1])]
  const nonIndexedCubeGeometry = [a, b, d, c, a, e, f, b, d, h, g, c, g, e, f, h]
  const flattenBox = [...(flatten(nonIndexedCubeGeometry) as any)] as number[]

  return {
    attribs: {
      position: { data: new Float32Array(flattenBox), size: 3 },
    },
    num: nonIndexedCubeGeometry.length,
    mode: gl.LINE_STRIP,
  }
}

export const getModelBox = (
  gl: WebGLRenderingContext,
  proj: GLMat4,
  view: GLMat4,
  shader: Shader,
) => {
  return compileModel(gl, <ModelSpec>{
    shader,
    uniforms: {
      color: [0.8, 0.8, 0.8, 1],
      proj,
      view,
    },
    ...boxGeometry(gl),
  })
}
