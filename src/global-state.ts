import { Atom, History } from '@thi.ng/atom'
import { setIn, getIn } from '@thi.ng/paths'
import * as tx from '@thi.ng/transducers'
import { SliderParam } from './components/slider'
import { Vec } from '@thi.ng/vectors'
import {
  computeNewData as computeNewData3d,
  allocateMemory as allocateMemory3d,
} from './buffers-3d'
import {
  computeNewData as computeNewData2d,
  allocateMemory as allocateMemory2d,
} from './buffers-2d'

// ------------------------------------------------------------------- types
// ------------------------------------------------------------------- types
// ------------------------------------------------------------------- types

export type TypeFN = (...x: any[]) => ([x01, y01]: [number, number]) => Vec

export type GlobalState = History<{
  numSliceRows: number
  numSliceColumns: number
  resolution: number
  selectedElevationFunction: number
  renderColumn: boolean
  renderRows: boolean
  params: {
    [functionId: number]: {
      [paramName: string]: SliderParam
    }
  }
  svgScale: number
}>

// ------------------------------------------------------------------- context
// ------------------------------------------------------------------- context
// ------------------------------------------------------------------- context

export function initContext(
  input: Array<{ id: number; name: string; params: SliderParam[]; fn: TypeFN }>,
) {
  console.log('init context')

  // ------------------------------------------------------------------- functions & sliders
  // ------------------------------------------------------------------- functions & sliders

  const { functions, params } = input.reduce(
    (acc, item) => {
      const { id, name, params, fn } = item

      acc.functions[id] = { name, fn }

      acc.params[id] = params.reduce((acc1, p) => {
        acc1[p.name] = p
        return acc1
      }, {})

      return acc
    },
    { functions: {}, params: {} } as {
      functions: { [key: number]: { fn: TypeFN; name: string } }
      params: { [functionId: number]: { [paramName: string]: SliderParam } }
    },
  )

  // ------------------------------------------------------------------- state
  // ------------------------------------------------------------------- state

  const state = new History(
    new Atom({
      numSliceRows: 4,
      numSliceColumns: 20,
      resolution: 80,
      selectedElevationFunction: 23,
      renderColumn: true,
      renderRows: true,
      params,
      svgScale: 60,
    }),
  )

  // ------------------------------------------------------------------- actions
  // ------------------------------------------------------------------- actions

  const actions = {
    setElevationFunction: (id: number) => {
      state.swap(st => setIn(st, 'selectedElevationFunction', id))
    },
    setParameter: (fnId: number, paramName: string, value: number) => {
      const path = [`params`, fnId, paramName, `value`]
      state.swap(st => setIn(st, path, value))
    },
    toggleCheckbox: (path: 'renderRows' | 'renderColumn') => {
      state.swap(st => setIn(st, path, !getIn(st, path)))
    },
  }

  // ------------------------------------------------------------------- computed
  // ------------------------------------------------------------------- computed

  // TODO: transform those computed thing in addViews
  const computed = {
    getAllParameters: (fnId: number) => {
      return [
        ...tx.transduce(
          tx.comp(
            tx.map(p => ['params', fnId, p.name, 'value']),
            tx.map(path => getIn(state.deref(), path)),
          ),
          tx.push(),
          Object.values(state.deref().params[fnId]),
        ),
      ]
    },
    getFnNamesById: () => {
      // QUESTION: this is not directly linked to the atom-state.
      // should be better to cache it?
      // better to put in the state?
      const fnNames = Object.entries(functions).map(([id, fn]) => [id, fn.name])
      return fnNames
    },
  }

  const context = {
    functions,
    params,
    state,
    actions,
    computed,
    data3d: allocateMemory3d(state),
    data2d: allocateMemory2d(state),
  }
  ;(window as any).context = context

  // ------------------------------------------------------------------- watch
  // ------------------------------------------------------------------- watch

  state.addWatch('compute-new-data', () => {
    computeNewData3d(context)
    computeNewData2d(context)
  })
  computeNewData3d(context)
  computeNewData2d(context)

  return context
}

export type Context = ReturnType<typeof initContext>
