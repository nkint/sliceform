import {
  transduce,
  comp,
  map,
  push,
  normRange,
  mapcat,
  partition,
  range,
  iterator,
  mapIndexed,
} from '@thi.ng/transducers'
import { DataMatrix } from './utils/data-matrix'
import { Vec, Vec3 } from '@thi.ng/vectors'
import { Context, GlobalState } from './global-state'

function readableBytes(bytes: number) {
  const i = Math.floor(Math.log(bytes) / Math.log(1024))
  const sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']

  return `${(bytes / Math.pow(1024, i)).toFixed(2)} ${sizes[i]}`
}

function computeIndices(lineLength: number, numLines: number) {
  const getLineIndex = (from: number, to: number) => [
    ...iterator(
      comp(
        partition(2, 1),
        mapcat(x => x),
      ),
      range(from, to),
    ),
  ]

  const indices: number[] = [
    ...iterator(
      comp(
        mapIndexed(i => getLineIndex(i * lineLength, i * lineLength + lineLength)),
        mapcat(x => x),
      ),
      range(numLines),
    ),
  ]

  return indices
}

export function allocateMemory(state: GlobalState) {
  const { resolution, numSliceRows, numSliceColumns } = state.deref()

  const k = 3 // number of points to complete the curve
  const lineLength = resolution + 1 + k

  const data = {
    matrix: null as DataMatrix,
    rows: [] as Vec[][],
    columns: [] as Vec[][],
    rowsBuffer: null as Float32Array,
    columnsBuffer: null as Float32Array,
    lineLength,
    rowsLength: (numSliceRows + 1) * lineLength,
    columnsLength: (numSliceColumns + 1) * lineLength,
    rowsIndices: new Uint16Array(computeIndices(lineLength, numSliceRows) as any),
    columnsIndices: new Uint16Array(computeIndices(lineLength, numSliceColumns) as any),
  }

  data.matrix = new DataMatrix(resolution, resolution)

  data.rowsBuffer = new Float32Array(data.rowsLength * 3)
  data.columnsBuffer = new Float32Array(data.columnsLength * 3)

  console.log('Allocated:')
  console.log(`${readableBytes(data.matrix.data.byteLength)} for matrix data`)
  console.log(
    `${readableBytes(
      data.rowsBuffer.byteLength +
        data.columnsBuffer.byteLength +
        data.rowsIndices.byteLength +
        data.columnsIndices.byteLength,
    )} for webgl positions and indices`,
  )

  return data
}

export function computeNewData(context: Context) {
  const { state, functions, computed, data3d: data } = context
  const { resolution, numSliceRows, numSliceColumns, selectedElevationFunction } = state.deref()

  const { fn } = functions[selectedElevationFunction]
  const sliderValues = computed.getAllParameters(selectedElevationFunction)

  data.matrix.compute(fn(...sliderValues))
  data.rows = getPoints(data.matrix, resolution, numSliceRows, 'row')
  data.columns = getPoints(data.matrix, resolution, numSliceColumns, 'column')

  Vec3.intoBuffer(data.rowsBuffer, mapcat(x => x, data.rows) as any)
  Vec3.intoBuffer(data.columnsBuffer, mapcat(x => x, data.columns) as any)
}

function getPoints(
  data: DataMatrix,
  resolution: number,
  numSlice: number,
  direction: 'column' | 'row',
) {
  return transduce(
    comp(
      map(x => {
        const index = Math.round(x * resolution)
        return [...data[direction](index)]
      }),
      map(line => {
        const firstElement: Vec3 = line[0]

        const bottomPoints: (Vec | Vec3)[] =
          direction === 'row'
            ? [[1, -1, firstElement[2]], [-1, -1, firstElement[2]], firstElement]
            : [[firstElement[0], -1, 1], [firstElement[0], -1, -1], firstElement]

        const lineExpanded = [...line, ...bottomPoints]
        return lineExpanded
      }),
    ),
    push<Vec[]>(),
    normRange(numSlice - 1),
  )
}
