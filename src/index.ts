import { start } from '@thi.ng/hdom'
import { getCanvas3d } from './components/canvas-3d'
import { getCanvas2d } from './components/canvas-2d'
import { title } from './components/title'
import { buttonList } from './components/fn-button-list'
import { sliderList } from './components/slider-list'
import { initContext, Context } from './global-state'
import { paramsFn1, getFn1, nameFn1 } from './functions/fn1'
import { paramsFn2, getFn2, nameFn2 } from './functions/fn2'
import { paramsFn3, getFn3, nameFn3 } from './functions/fn3'
import { checkboxList } from './components/checkbox-list'
import 'tachyons'

let canvas3d: ReturnType<typeof getCanvas3d> = getCanvas3d()
let canvas2d: ReturnType<typeof getCanvas2d> = getCanvas2d()

export const app = (context: Context) => {
  const fnNamesById = context.computed.getFnNamesById()

  return () => {
    const isCanvasReady = canvas3d && canvas2d
    const { data3d: data, state } = context
    const { selectedElevationFunction, params } = state.deref()

    return [
      'div.ma3.f6',
      {
        style: {
          'font-family': "'Consolas', 'Courier New', 'Courier', monospace;",
        },
      },
      [
        title,

        ['div.mt4.mr3', buttonList(context, selectedElevationFunction, fnNamesById)],

        isCanvasReady && [
          'div',
          { class: 'flex' },

          [canvas3d, { width: 550, height: 550, class: 'pa2 mv3' }],

          [
            'div.mt4',
            checkboxList(context),
            sliderList(context, selectedElevationFunction, params[selectedElevationFunction]),
          ],
        ],

        [canvas2d, { width: 600, class: 'ba pa2 ml2' }],
      ],
    ]
  }
}

const context = initContext([
  { id: 23, name: nameFn3, params: paramsFn3, fn: getFn3 },
  { id: 24, name: nameFn2, params: paramsFn2, fn: getFn2 },
  { id: 25, name: nameFn1, params: paramsFn1, fn: getFn1 },
])

const cancel = start(app(context), { ctx: context })

if (process.env.NODE_ENV !== 'production') {
  const hot = (<any>module).hot
  hot && hot.dispose(cancel)
}
