import { Vec, Vec3 } from '@thi.ng/vectors'
import { normRange2 } from './norm-range2'
import { flatten, map } from '@thi.ng/transducers'

export class DataMatrix {
  data: Float32Array
  colNum: number
  rowNum: number

  constructor(col: number, row: number) {
    this.colNum = col
    this.rowNum = row

    const dataSize = (col + 1) * (row + 1) * 3
    this.data = new Float32Array(dataSize)
  }

  compute(fn: ([x01, y01]: [number, number]) => Vec) {
    this.data.set([...flatten(map(fn, normRange2(this.colNum, this.rowNum)))] as any)
  }

  get width() {
    return this.colNum
  }

  get height() {
    return this.rowNum
  }

  at(x: number, y: number) {
    const index = (x + y * (this.colNum + 1)) * 3
    return new Vec3(this.data, index)
  }

  *row(index: number) {
    const y = index
    for (let x = 0; x <= this.colNum; x++) {
      yield this.at(x, y)
    }
  }

  *column(index: number) {
    const x = index
    for (let y = 0; y <= this.rowNum; y++) {
      yield this.at(x, y)
    }
  }
}
