import { polyline, line } from '@thi.ng/hiccup-svg'
import { ILifecycle } from '@thi.ng/hdom'
import { Context } from '../global-state'
import { OffsetLine, Ticks } from '../buffers-2d'

function renderSlice(line2d: OffsetLine[], ticks: Ticks[], colorTicks: string) {
  return line2d.map((r, i) => [
    'g',
    { transform: `translate(0 ${r.offset})` },
    polyline(r.line, {
      stroke: 'black',
      fill: 'none',
    }),
    ...ticks[i].map(({ p0, p1 }) => line(p0, p1, { stroke: colorTicks })),
  ])
}

export function getCanvas2d() {
  const svgCanvas: ILifecycle = {
    // init(el: Element, ctx: any, ...args: any[]) {},
    // release(ctx: any, ...args: any[]) {},
    render(ctx: Context, ...args: any[]) {
      const attribs: {
        width: number
        class: string
      } = args[0]

      const { svgScale } = ctx.state.deref()

      const marginLeft = (attribs.width - svgScale * 4 - 30) / 2
      const { rows2d, columns2d, rowsTicks, columnsTicks } = ctx.data2d
      return [
        'svg',
        {
          ...attribs,
          // height: Math.max(rows.length, columns.length) * (svgScale + 30)
          height: attribs.width * 2,
        },
        [
          'g',
          { transform: `translate(${marginLeft} ${marginLeft})` },
          [
            'g.rows',
            { transform: `translate(${svgScale} ${svgScale})` },
            ...renderSlice(rows2d, columnsTicks, 'red'),
          ],
          [
            'g.columns',
            { transform: `translate(${svgScale + svgScale * 2 + 30} ${svgScale})` },
            ...renderSlice(columns2d, rowsTicks, 'orange'),
          ],
        ],
      ]
    },
  }

  return svgCanvas
}
