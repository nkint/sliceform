import { canvasWebGL } from '@thi.ng/hdom-components'
import { lookAt, perspective, concat, rotationX44, rotationY44 } from '@thi.ng/matrices'
import { draw, GLMat4, ModelSpec, shader } from '@thi.ng/webgl'
import { getModelAxis } from '../webgl-components/axis'
import { getModelBox } from '../webgl-components/box'
import { modelLineBuilder } from '../webgl-components/line'
import { simpleShader } from '../webgl-components/simple-shader'
import { Vec } from '@thi.ng/vectors'
import { Context } from '../global-state'

type ArgumentCanvas3d = {
  key: number
  rows: Vec[][]
  columns: Vec[][]
}

const rotateModel = (modelSpec: ModelSpec, rotationTime: number) => {
  modelSpec.uniforms!.model = <GLMat4>concat([], rotationX44([], 0), rotationY44([], rotationTime))
}

export function getCanvas3d() {
  let modelRows: ModelSpec
  let modelColumns: ModelSpec
  let modelBox: ModelSpec
  let modelAxis: ModelSpec

  const proj = <GLMat4>perspective([], 60, 1, 0.1, 10)
  const view = <GLMat4>lookAt([], [2, 2, 2], [0, 0, 0], [0, 1, 0])

  const canvas = canvasWebGL({
    init(el, gl, ctx: Context) {
      console.log('canvasWebGL > init')

      const lineUniforms = {
        proj,
        view,
        color: [0, 0, 0, 1],
      }

      const lineShader = shader(gl, simpleShader)

      const { state, data3d: data } = ctx
      state.addWatch('elevationCanvas3d', () => {
        // QUESTION: is there a better way to do this other then addWatch?

        modelRows.attribs.position.buffer.set(data.rowsBuffer)
        modelColumns.attribs.position.buffer.set(data.columnsBuffer)
      })

      const { rowsBuffer, rowsIndices, columnsBuffer, columnsIndices } = data
      modelRows = modelLineBuilder(gl, rowsBuffer, rowsIndices, lineUniforms, lineShader)
      modelColumns = modelLineBuilder(gl, columnsBuffer, columnsIndices, lineUniforms, lineShader)

      modelBox = getModelBox(gl, proj, view, lineShader)
      modelAxis = getModelAxis(gl, proj, view)
    },
    release() {
      console.log('release')
    },
    update(_, gl, context, time, frame, args: ArgumentCanvas3d) {
      time *= 0.001
      const rotationTime = time * 0.3

      const { renderRows, renderColumn } = (context as Context).state.deref()

      rotateModel(modelRows, rotationTime)
      rotateModel(modelColumns, rotationTime)
      rotateModel(modelBox, rotationTime)
      rotateModel(modelAxis, rotationTime)

      gl.clearColor(1, 1, 1, 1)
      gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
      draw(modelBox)
      draw(modelAxis)
      renderRows && draw(modelRows)
      renderColumn && draw(modelColumns)
    },
  })

  return canvas
}
