import { Context } from '../global-state'

function fnButton(borderColor: string, name: string, onclick: () => void) {
  return [
    'div',
    {
      class: 'ba br2 mr2 flex items-center justify-center ph2 pointer',
      style: { height: '40px', 'border-color': borderColor },
      onclick,
    },
    ['p.ma0', { style: { 'vertical-align': 'middle' } }, name],
  ]
}

export function buttonList(context: Context, selected: number, names: string[][]) {
  return [
    'div.flex',
    ...names.map(([id, name]) => {
      const borderColor = Number(id) === selected ? 'dark-gray' : 'silver'
      const onclick = () => {
        context.actions.setElevationFunction(Number(id))
      }
      return fnButton(borderColor, name, onclick)
    }),
  ]
}
