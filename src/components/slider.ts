import { Cursor } from '@thi.ng/atom'

export type SliderParam = {
  type: 'range'
  min: number
  max: number
  value: number
  name: string
}

export function slider(param: SliderParam, onChange: (x: number) => void) {
  return [
    'div.mb3',
    { style: { width: '200px' } },
    ['p', { style: { margin: '0 auto', width: '100px' } }, `${param.name}: `, param.value],
    [
      'input',
      {
        ...param,
        step: 0.1,
        oninput: (e: InputEvent) => {
          const value = Number((e.target as HTMLInputElement).value)
          onChange(value)
        },
      },
    ],
  ]
}
