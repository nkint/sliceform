import { Context } from '../global-state'

export function checkboxList(context: Context) {
  const { actions, state } = context

  return [
    [
      'div',
      [
        'input',
        {
          onchange: () => actions.toggleCheckbox('renderRows'),
          type: 'checkbox',
          checked: state.deref().renderRows,
        },
      ],
      ['span', 'render rows'],
    ],
    [
      'div',
      [
        'input',
        {
          onchange: () => actions.toggleCheckbox('renderColumn'),
          type: 'checkbox',
          checked: state.deref().renderColumn,
        },
      ],
      ['span', 'render columns'],
    ],
  ]
}
