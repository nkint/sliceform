import { SliderParam, slider } from './slider'
import { Context } from '../global-state'

export const sliderList = (
  context: Context,
  fnId: number,
  params: { [paramName: string]: SliderParam },
) => {
  // QUESTION: how to optimize this component and execute it only when the fn change?
  const sliderParams = Object.values(params)
  return [
    'div',
    ...sliderParams.map(param => {
      const onChange = (x: number) => context.actions.setParameter(fnId, param.name, x)
      return slider(param, onChange)
    }),
  ]
}
