export const title = [
  'div',
  [
    'h1',
    { class: 'mb2' },
    [
      ['span', 'slice'],
      [
        'span',
        {
          class: 'code',
          style: { 'margin-right': '-2px', 'margin-left': '1px' },
        },
        'ƒ',
      ],
      ['span', 'orm'],
    ],
  ],
  // ['p.b.code', 'Slice of mathematical surface'],
  // ['hr'],
]
